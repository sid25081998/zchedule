package com.example.sridh.vdiary.Activities;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;

import android.os.Process;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.sridh.vdiary.Classes.Subject;
import com.example.sridh.vdiary.Classes.Holiday;
import com.example.sridh.vdiary.Classes.Teacher;
import com.example.sridh.vdiary.Classes.themeProperty;
import com.example.sridh.vdiary.Classes.Notification_Holder;
import com.example.sridh.vdiary.List_Adapters.CourseAdapter;
import com.example.sridh.vdiary.List_Adapters.listAdapter_teachers;
import com.example.sridh.vdiary.Receivers.NotifyService;
import com.example.sridh.vdiary.R;
import com.example.sridh.vdiary.Utils.DataContainer;
import com.example.sridh.vdiary.config;
import com.example.sridh.vdiary.Utils.prefs;
import com.example.sridh.vdiary.Widget.widgetServiceReceiver;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import 	android.support.v7.widget.RecyclerView.ItemDecoration;

import static com.example.sridh.vdiary.Utils.prefs.*;
import static com.example.sridh.vdiary.config.CurrentTheme;
import static com.example.sridh.vdiary.config.getCurrentTheme;


public class workSpace extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */

    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */

    private ViewPager mViewPager;
    static Context context;

    static int id = 1000;


    static ListView resultList;
    static EditText teacherSearch;

    List<String> attList = new ArrayList<>();
    List<String> ctdList = new ArrayList<>();
    List<String> attendedList = new ArrayList<>();
    List<Subject> courses = new ArrayList<>();
    List<List<Subject>> scheduleList = new ArrayList<>();
    Gson jsonBuilder= new Gson();
    boolean attendanceStatus=true;
    public static boolean refreshing=false;
    public static boolean refreshedByScrapper=false;

    ProgressBar pb_syncing;
    ImageButton action_sync;

    public static TextView currentShowSubjectTextView=null;
    public static int currentShowing = -1;

    public static showSubject currentInView=null;

    boolean isPasswordChanged=false;
    static themeProperty ThemeProperty ;
    static ArrayAdapter<String> adapter;


    @Override
    public void onBackPressed() {
        if (resultList.getVisibility() == View.VISIBLE) {
            resultList.setVisibility(View.INVISIBLE);
        } else finish();
        return;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;

        CurrentTheme = prefs.getTheme(context); //GETTING THE THEME FROM THE SHARED PREFERENCES
        ThemeProperty= getCurrentTheme();
        setTheme(ThemeProperty.theme);

        View rootView = getLayoutInflater().inflate(R.layout.activity_workspace,null);
        setContentView(rootView);
        setOnTouchListener(rootView,workSpace.this);
        config.getFonts(context);
        getDimensions();
        id = get(context,notificationIdentifier,1000);

        readFromPrefs(getApplicationContext());  //set all the data to the environment variables



        String z = get(context,lastRefreshed,"");//s.getString("last_ref", "");
        if (!z.equals("")) {
            try {
                Calendar lastSynced = new Gson().fromJson(z, new TypeToken<Calendar>() {
                }.getType());
                Toast.makeText(context, "Last synced on " + getDateTimeString(lastSynced), Toast.LENGTH_LONG).show();
            }
            catch (Exception e){
                Toast.makeText(context, "Last synced on " + z, Toast.LENGTH_LONG).show();
            }
        }
        String get_list = get(context,todolist,null);//shared.getString("todolist", null);
        if (get_list != null) {
            DataContainer.notes = Notification_Holder.convert_from_jason(get_list);
        }
        //config.notes is initialized
        setToolbars();
        //shared.getInt("notificationIdentifier", 1000);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(2);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        setTabLayout(tabLayout);
        if(!refreshedByScrapper){
            refreshing=true;
            initWebViews();
        }
        else{
            pb_syncing.setVisibility(View.VISIBLE);
            action_sync.setVisibility(View.GONE);
        }
    }

    boolean readFromPrefs(Context context){
        Gson jsonBuilder = new Gson();
        String allSubJson = get(context,allSub,null); //academicPrefs.getString("allSub",null);
        String scheduleJson = get(context,schedule,null);//academicPrefs.getString("schedule",null);
        String teachersJson = get(context,teachers,null);//teacherPrefs.getString("teachers",null);
        String holidaysJson = get(context,holidays,null);//holidayPrefs.getString("holidays",null);
        String customTeachersJson = get(context,customTeachers,null);//teacherPrefs.getString("customTeachers",null);
        if(customTeachersJson!=null){
            DataContainer.cablist=jsonBuilder.fromJson(customTeachersJson,new TypeToken<List<Teacher>>(){}.getType());
        }
        if(teachersJson!=null){
            DataContainer.teachers = jsonBuilder.fromJson(teachersJson,new TypeToken<Map<String,Teacher>>(){}.getType());
        }
        if(holidaysJson!=null){
            DataContainer.holidays=jsonBuilder.fromJson(holidaysJson,new TypeToken<List<Holiday>>(){}.getType());
        }
        if(allSubJson!=null && scheduleJson!=null){
            DataContainer.subList=jsonBuilder.fromJson(allSubJson,new TypeToken<ArrayList<Subject>>(){}.getType());
            DataContainer.timeTable=jsonBuilder.fromJson(scheduleJson, new TypeToken<ArrayList<ArrayList<Subject>>>(){}.getType());
            return true;
        }
        return false;
    } //READ ACADEMIC CONTENT FROM SHARED PREFERENCE



    private void initWebViews(){
        pb_syncing.setVisibility(View.VISIBLE);
        action_sync.setVisibility(View.GONE);
        attList = new ArrayList<>();
        ctdList = new ArrayList<>();
        attendedList = new ArrayList<>();
        courses = new ArrayList<>();
        scheduleList = new ArrayList<>();

    } //INITIALIZE THE WEBVIEWS AND LAST LOADING THE LOGIN PAGE


    class compileInf extends AsyncTask<Void,Void,Void>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            float sum=0;
            DataContainer.subList=courses;
            DataContainer.timeTable=scheduleList;
            for (int i=0;i<courses.size();i++){
                int ctd,attended;
                String attString;
                if(!attendanceStatus){
                    ctd=0;
                    attString=String.valueOf(0);
                    attended=0;
                }
                else{
                    ctd=Integer.parseInt(ctdList.get(i));
                    attString = attList.get(i)+"%";
                    attended=Integer.parseInt(attendedList.get(i));
                    sum+=Integer.parseInt(attList.get(i));
                }
                DataContainer.subList.get(i).ctd=ctd;
                DataContainer.subList.get(i).attString=attString;
                DataContainer.subList.get(i).classAttended=attended;
            }
            try{
                sum=sum/ DataContainer.subList.size();
                put(context,avgAttendance,((int)(Math.ceil(sum))));
            }
            catch (Exception e){
                //ATTENDANCE NOT YET UPLOADED
            }
            for (List<Subject> i : DataContainer.timeTable) {
                for (int count = 0; count < i.size(); count++) {
                    Subject sub = getSubject(i.get(count).code, i.get(count).type);
                    if (sub != null) {
                        i.get(count).attString = sub.attString;
                        i.get(count).teacher = sub.teacher;
                        i.get(count).title = sub.title;
                        i.get(count).room = sub.room;
                        if (sub.type.equals("ELA") || sub.type.equals("LO")) {
                            i.remove(count + 1);
                            placeCorrectly(i.get(count), i);
                        }
                    }
                }
            }
            writeToPrefs();
            cancelNotifications(context);
            createNotification(context, DataContainer.timeTable);
            put(context,lastRefreshed,(new Gson()).toJson(Calendar.getInstance()));//editor.putString("last_ref",last_ref);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                //courseAdapter.update(DataContainer.subList);
            }
            catch (Exception e){
                //COURSE ADAPTER NIT YET READY
            }
            refreshing=false;
            config.isSyncedThisSession=true;
        }
    } //REARRANGE THE INFORMATION SCRAPPED FORM THE WEBPAGE

    public static void createNotification(Context context,List<List<Subject>> timeTable){
        int day=2;
        int notificationCode=1;
        for(List<Subject> today: timeTable){
            for (Subject sub : today){
                if(!sub.code.equals("")){
                    AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                    Intent toNotifyService = new Intent(context,NotifyService.class);
                    toNotifyService.putExtra("fromClass","scheduleNotification");
                    Calendar calendar = Calendar.getInstance();
                    int startHour,startMin,AMPM;
                    String time=formattedTime(sub);
                    startHour=Integer.parseInt(time.substring(0, 2));
                    startMin=Integer.parseInt(time.substring(3, 5));

                    calendar.setLenient(false);
                    calendar.set(Calendar.HOUR_OF_DAY,startHour);
                    calendar.set(Calendar.MINUTE,startMin);
                    calendar.set(Calendar.DAY_OF_WEEK,day);
                    calendar.set(Calendar.SECOND,0);

                    Notification_Holder newNotification =  new Notification_Holder(calendar,sub.title,sub.room,"Upcoming class in 5 minutes");
                    toNotifyService.putExtra("notificationContent",(new Gson()).toJson(newNotification));
                    toNotifyService.putExtra("notificationCode",notificationCode);
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(context,notificationCode,toNotifyService,PendingIntent.FLAG_UPDATE_CURRENT);
                    notificationCode++;
                    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis() - 5 * 60 * 1000, 24 * 7 * 60 * 60 * 1000, pendingIntent);
                }
            }
            day++;
        }
    }

    private Subject getSubject(String code, String type){
        for(Subject i: DataContainer.subList){
            if(i.code.equals(code) && i.type.equals(type)){
                return i;
            }
        }
        return null;
    } //SEARCH SUBJECT IN SUBJECT LIST

    void writeToPrefs(){
        if(get(context,isLoggedIn,false)) {
            put(context, allSub, jsonBuilder.toJson(DataContainer.subList));//editor.putString("allSub",jsonBuilder.toJson(config.subList));
            put(context, schedule, jsonBuilder.toJson(DataContainer.timeTable));//editor.putString("schedule",jsonBuilder.toJson(config.timeTable));
            updateWidget();
        }
    } //WRITE ACADEMIC CONTENT TO SHARED PREFERENCES

    static void updateWidget(){
        (new widgetServiceReceiver()).onReceive(context,(new Intent(context,widgetServiceReceiver.class)));
    }  // UPDATE THE CONTENTS OF WIDGET TO SHOW TODAYS SCHEDULE

    static String formattedTime(Subject sub){
        String rawTime= sub.startTime;
        String meridian =rawTime.substring(6,8);
        int hour = Integer.parseInt(rawTime.substring(0,2));
        if(meridian.equals("PM") && hour<12){
            hour = hour+12;
            String t=hour+rawTime.substring(2);
            return t;
        }
        return rawTime;
    } //GET THE 24-HOUR FORMAT OF THE TIME OF THE SUBJECT

    void placeCorrectly(Subject sub, List<Subject> i){
        i.remove(sub);
        int subHour = Integer.parseInt(formattedTime(sub).substring(0,2));
        int subMin=Integer.parseInt(formattedTime(sub).substring(3,5));
        for(int count=0;count<i.size();count++){
            int checkHour =Integer.parseInt(formattedTime(i.get(count)).substring(0,2));
            if(subHour==checkHour){
                int checkMin =Integer.parseInt(formattedTime(i.get(count)).substring(3,5));
                if(subMin<checkMin){
                    i.add(count,sub);
                    return;
                }
            }
            else if(subHour<checkHour){
                i.add(count,sub);
                return;
            }
        }
    } //PLACE THE LAB IN THERE CORRECT POSITION BY INSERTION SORT

    void setTabLayout(TabLayout tabLayout){
        /*GradientDrawable softShape = (GradientDrawable) tabLayout.getBackground();
        softShape.setColor(getResources().getColor(ThemeProperty.colorPrimary));*/
        final int[] unselectedDrawables= new int[]{R.drawable.notselected_course_book,R.drawable.notselected_teacher,R.drawable.notselected_tasks,R.drawable.notselected_summary};
        final int[] selectedDrawables = new int[]{R.drawable.selected_course_book,R.drawable.selected_teacher,R.drawable.selected_tasks,R.drawable.selected_summary};
        for(int i=1;i<4;i++){
            tabLayout.getTabAt(i).setIcon(unselectedDrawables[i]);
        }
        tabLayout.getTabAt(0).setIcon(R.drawable.selected_course_book);
        //TABICONS INITIALISED
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                tab.setIcon(selectedDrawables[position]);
                mViewPager.setCurrentItem(position);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                tab.setIcon(unselectedDrawables[position]);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }  //CONTROLS THE IMAGES WHILE SWITCHING THE TABS

    static void delPrefs(){
        put(context,allSub,null);//editor.putString("allSub",jsonBuilder.toJson(config.subList));
        put(context,schedule,null);//editor.putString("schedule",jsonBuilder.toJson(config.timeTable));
        put(context,isLoggedIn,false);//editor.putBoolean("isLoggedIn",true);
        DataContainer.timeTable= new ArrayList<>();
        DataContainer.subList = new ArrayList<>();
        updateWidget();
    }  //DELETES THE PREFERENCES WHEN LOGOUT IS PRESSED

    void setToolbars() {
        AppBarLayout appBarLayout = (AppBarLayout)findViewById(R.id.appbar);
        GradientDrawable softShape = (GradientDrawable) appBarLayout.getBackground();
        softShape.setColor(getResources().getColor(ThemeProperty.colorPrimary));
        Toolbar toolbar = (Toolbar) findViewById(R.id.workspacetoptoolbar);
        pb_syncing=(ProgressBar)toolbar.findViewById(R.id.pb_syncing);
        action_sync=(ImageButton)toolbar.findViewById(R.id.action_sync);
        action_sync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!refreshing && !config.isSyncedThisSession){
                    refreshing=true;
                    //TODO GET FROM API
                }
            }
        });
        toolbar.inflateMenu(R.menu.menu_workspace_top);
        toolbar.setBackgroundColor(getResources().getColor(ThemeProperty.colorPrimary));
        TextView title = (TextView)toolbar.findViewById(R.id.workSpace_title);
        title.setTypeface(config.fredoka);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                switch (id){
                    case R.id.toSchedule:
                        Intent i = new Intent(workSpace.this, com.example.sridh.vdiary.Activities.schedule.class);
                        startActivity(i);
                        break;
                }
                return true;
            }
        });
    }  //SET THE TOOLBARS FOR THE WORKSPACE CLASS



    public static void confirmLogout(final Context context,final Activity activity){
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        cancelNotifications(context);
                        delPrefs();
                        currentShowing=-1;
                        currentInView=null;
                        currentShowSubjectTextView=null;
                        context.startActivity(new Intent(context, scrapper.class));
                        activity.overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_up);
                        activity.finish();
                        try {
                            this.finalize();
                        } catch (Throwable throwable) {
                            //throwable.printStackTrace();
                        }
                        break;
                }
            }
        };

        builder.setMessage("Doing this will delete all data!\n\nAre you sure?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener);
        AlertDialog confirmLogoutDialog = builder.create();
        confirmLogoutDialog.show();
    }  //ASK FOR CONFIRMATION TO LOGOUT

    static void cancelNotifications(Context context) {
        int day=2;
        int notificationCode=1;
        for(List<Subject> today: DataContainer.timeTable){
            for (Subject sub : today){
                if(!sub.code.equals("")){
                    AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                    Intent toNotifyService = new Intent(context,NotifyService.class);
                    toNotifyService.putExtra("fromClass","scheduleNotification");
                    Calendar calendar = GregorianCalendar.getInstance();
                    int startHour,startMin;
                    String time=formattedTime(sub);
                    startHour=Integer.parseInt(time.substring(0, 2));
                    startMin=Integer.parseInt(time.substring(3, 5));

                    calendar.setLenient(false);
                    calendar.set(GregorianCalendar.HOUR_OF_DAY,startHour);
                    calendar.set(GregorianCalendar.MINUTE,startMin);
                    calendar.set(GregorianCalendar.DAY_OF_WEEK,day);
                    calendar.set(GregorianCalendar.SECOND,0);

                    Notification_Holder newNotification =  new Notification_Holder(calendar,sub.title,sub.room,"Upcoming class in 5 minutes");
                    toNotifyService.putExtra("notificationContent",(new Gson()).toJson(newNotification));
                    toNotifyService.putExtra("notificationCode",notificationCode);
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(context,notificationCode,toNotifyService,PendingIntent.FLAG_UPDATE_CURRENT);
                    alarmManager.cancel(pendingIntent);
                    notificationCode++;
                }
            }
            day++;
        }
    } //CANCEL ALL THE NOTIFICATION OF THE SUBJECTS



    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        TextView noNotesText;

        public PlaceholderFragment(){
        }
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                                 final Bundle savedInstanceState) {
            switch (getArguments().getInt(ARG_SECTION_NUMBER) - 1) {
                case 0:
                    View rootViewCourse = inflater.inflate(R.layout.fragment_courses, container, false);
                    setOnTouchListener(rootViewCourse,getActivity());
                    final RecyclerView lview = (RecyclerView) rootViewCourse.findViewById(R.id.course_listview);
                    final CourseAdapter cadd = new CourseAdapter(context, DataContainer.subList);
                    lview.setAdapter(cadd);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(context);
                    lview.setLayoutManager(layoutManager);
                    lview.setItemAnimator(new DefaultItemAnimator());
                    cadd.setOnItemClickListener(new CourseAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(Subject subject, int position) {
                            workSpace.currentShowing=position;
                            Intent showSubjectIntent = new Intent(context, showSubject.class);
                            showSubjectIntent.putExtra("position", position);
                            //cadd.change(position);
                            startActivity(showSubjectIntent);
                        }
                    });
                    final SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) rootViewCourse.findViewById(R.id.course_refresh);
                    swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                        @Override
                        public void onRefresh() {
                            //TODO ADD GET FROM API HERE
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    });
                    swipeRefreshLayout.setColorSchemeResources(ThemeProperty.colorPrimaryDark);
                    return rootViewCourse;
                case 1:
                    View rootViewteachers = inflater.inflate(R.layout.fragment_teachers, container, false);
                    teacherSearch = (EditText) rootViewteachers.findViewById(R.id.teachers_searchText);
                    resultList = (ListView) rootViewteachers.findViewById(R.id.teachers_search_list);
                    resultList.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View view, MotionEvent motionEvent) {
                            hideSoftKeyboard(getActivity());
                            return false;
                        }
                    });
                    setOnViewTouchListener(rootViewteachers,getActivity());
                    FloatingActionButton fab = (FloatingActionButton) rootViewteachers.findViewById(R.id.teachers_add);
                    fab.setBackgroundTintList(getResources().getColorStateList(ThemeProperty.colorPrimary));
                    ListView lv = (ListView) rootViewteachers.findViewById(R.id.teachers_list);
                    setOnTouchListener(lv,getActivity());
                    final listAdapter_teachers mad = new listAdapter_teachers(context, DataContainer.cablist);
                    ImageButton button = (ImageButton)rootViewteachers.findViewById(R.id.iv_search);
                    setSearcher(teacherSearch,mad,button);
                    lv.setAdapter(mad);
                    fab.setOnClickListener(new View.OnClickListener() { //Onclick Listener for floating action Button
                        @Override
                        public void onClick(View v) {
                            showCabinAlertDialog(mad);
                        }
                    });
                    return rootViewteachers;
                case 2:
                    View rootViewNotes = inflater.inflate(R.layout.fragment_notes, container, false);
                    setOnTouchListener(rootViewNotes,getActivity());
                    taskGridLeft = (LinearLayout) rootViewNotes.findViewById(R.id.task_grid_view_left);
                    int taskViewWidth = ((int) (config.width * 0.492));
                    taskGridLeft.getLayoutParams().width = taskViewWidth;
                    taskGridRight = (LinearLayout) rootViewNotes.findViewById(R.id.task_grid_view_right);
                    taskGridRight.getLayoutParams().width = taskViewWidth;
                    noNotesText = (TextView)rootViewNotes.findViewById(R.id.ifNoteExists);
                    noNotesText.setTypeface(config.nunito_bold);
                    populateTaskGrid();
                    FloatingActionButton fb = (FloatingActionButton) rootViewNotes.findViewById(R.id.notes_add);
                    fb.setBackgroundTintList(getResources().getColorStateList(ThemeProperty.colorPrimary));
                    fb.setOnClickListener(new View.OnClickListener() { //Floating action button onclick listener
                        @Override
                        public void onClick(View v) {
                            final AlertDialog alert;
                            View root = getActivity().getLayoutInflater().inflate(R.layout.floatingview_add_todo, null);
                            final EditText title = (EditText) root.findViewById(R.id.title);
                            final EditText other = (EditText) root.findViewById(R.id.note);
                            final Switch reminderSwitch =(Switch)root.findViewById(R.id.add_todo_reminder_switch);
                            Toolbar addTaskToolbar=((Toolbar)root.findViewById(R.id.add_task_toolbar));
                            addTaskToolbar.inflateMenu(R.menu.menu_add_todo);
                            reminderSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                                    if(checked) {
                                        c = null;
                                        showReminderSetter(reminderSwitch);
                                    }
                                    else {
                                        c = null;
                                        reminderSwitch.setText("Set Reminder");
                                    }
                                }
                            });
                            AlertDialog.Builder bui = new AlertDialog.Builder(context);
                            bui.setView(root);
                            alert = bui.create();
                            alert.show();
                            alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialogInterface) {
                                    hideSoftKeyboard(getActivity());
                                }
                            });
                            addTaskToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
                                @Override
                                public boolean onMenuItemClick(MenuItem item) {
                                    int id = item.getItemId();
                                    if(id==R.id.action_add_todo) {
                                        if (!title.getText().toString().isEmpty() && !other.getText().toString().isEmpty()) {
                                            Notification_Holder n;
                                            if(c!=null) {
                                                n = new Notification_Holder(c, title.getText().toString(), other.getText().toString(),"You have a deadline to meet");
                                                schedule_todo_notification(n);
                                                c=null;
                                            }
                                            else
                                                n = new Notification_Holder(Calendar.getInstance(), title.getText().toString(), other.getText().toString(),"You have a deadline to meet");
                                            DataContainer.notes.add(n);
                                            populateTaskGrid();
                                            Gson json = new Gson();
                                            String temporary = json.toJson(DataContainer.notes);
                                            put(context,todolist,temporary);//editor.putString("todolist", temporary);
                                            alert.cancel();
                                            workSpace.hideSoftKeyboard(getActivity());
                                        } else
                                            Toast.makeText(getContext(), "Both title and note must contain some text", Toast.LENGTH_SHORT).show();
                                        return true;
                                    }
                                    else if(id== R.id.action_cancel_todo){
                                        workSpace.hideSoftKeyboard(getActivity());
                                        alert.cancel();
                                    }
                                    return false;
                                }
                            });
                        }
                    });
                    return rootViewNotes;
                case 3:
                    return getSummaryView();
            }
            return null;
        }

        View getSummaryView(){
            View rootViewSummary = getActivity().getLayoutInflater().inflate(R.layout.fragment_summary,null);
            setOnTouchListener(rootViewSummary,getActivity());
            PieChart pie = (PieChart)rootViewSummary.findViewById(R.id.avgAtt);
            pie.setLayoutParams(new RelativeLayout.LayoutParams(((int)(config.width*0.53)),((int)(config.height*0.35))));
            int avg = get(context,avgAttendance,0);
            pie.setCenterText("Avg\n"+get(context,avgAttendance,0)+"%");
            pie.setCenterTextTypeface(config.nunito_Extrabold);
            if(avg<75 ) pie.setCenterTextColor(Color.RED);
            else pie.setCenterTextColor(Color.BLACK);
            pie.setCenterTextSize(25);
            ArrayList<Entry> pieEntry= new ArrayList<>();
            pieEntry.add(new Entry(avg,0));
            pieEntry.add(new Entry(100-avg,1));
            ArrayList<String> labels=new ArrayList<>();
            labels.add("");
            labels.add("");
            PieDataSet dataSet = new PieDataSet(pieEntry,"");
            dataSet.setColors(ColorTemplate.createColors(getResources(),new int[]{ThemeProperty.colorPrimaryDark,ThemeProperty.colorPrimary})); //TODO APPLY CHNAGES ACORDING TO THE THEME OF THE APP
            PieData data = new PieData(labels,dataSet);
            pie.setData(data);
            pie.setDescription("");
            pie.setDrawSliceText(false);
            data.setDrawValues(false);
            pie.getLegend().setEnabled(false);

            TextView lastRef= (TextView)rootViewSummary.findViewById(R.id.lastRefreshed);
            lastRef.setTypeface(config.nunito_bold);
            String lastSyncedJson = get(context,lastRefreshed,"");
            try {
                Calendar lastSynced = new Gson().fromJson(lastSyncedJson, new TypeToken<Calendar>() {
                }.getType());
                lastRef.setText("Last Synced:\n" + getDateTimeString(lastSynced));
            }
            catch (Exception e){
                lastRef.setText("Last Synced:\n" +lastSyncedJson);
            }

            RelativeLayout logoutButt = (RelativeLayout)rootViewSummary.findViewById(R.id.rl_logout);
            TextView logoutText = (TextView)rootViewSummary.findViewById(R.id.tv_logout);
            logoutText.setTextColor(getResources().getColor(ThemeProperty.colorPrimaryDark));
            logoutText.setTypeface(config.nunito_reg);
            ImageView iv_logout =(ImageView)logoutButt.findViewById(R.id.iv_logout);
            iv_logout.setColorFilter(getResources().getColor(ThemeProperty.colorPrimaryDark));
            logoutButt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    workSpace.confirmLogout(context,getActivity());
                }
            });

            RelativeLayout aboutButt = (RelativeLayout)rootViewSummary.findViewById(R.id.rl_about);
            TextView aboutText = (TextView)rootViewSummary.findViewById(R.id.tv_about);
            aboutText.setTextColor(getResources().getColor(ThemeProperty.colorPrimaryDark));
            aboutText.setTypeface(config.nunito_reg);
            ImageView iv_about =(ImageView)aboutButt.findViewById(R.id.iv_about);
            iv_about.setColorFilter(getResources().getColor(ThemeProperty.colorPrimaryDark));
            aboutButt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(context,About.class));
                }
            });

            RelativeLayout settingButt = (RelativeLayout)rootViewSummary.findViewById(R.id.rl_setting);
            TextView settingText = (TextView)rootViewSummary.findViewById(R.id.tv_setting);
            settingText.setTextColor(getResources().getColor(ThemeProperty.colorPrimaryDark));
            settingText.setTypeface(config.nunito_reg);
            ImageView iv_setting =(ImageView)settingButt.findViewById(R.id.iv_setting);
            iv_setting.setColorFilter(getResources().getColor(ThemeProperty.colorPrimaryDark));
            settingButt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(context,settings.class));
                }
            });

            RelativeLayout shareButt = (RelativeLayout)rootViewSummary.findViewById(R.id.rl_share);
            TextView shareText = (TextView)rootViewSummary.findViewById(R.id.tv_share);
            shareText.setTextColor(getResources().getColor(ThemeProperty.colorPrimaryDark));
            shareText.setTypeface(config.nunito_reg);
            ImageView iv_share =(ImageView)shareButt.findViewById(R.id.iv_share);
            iv_share.setColorFilter(getResources().getColor(ThemeProperty.colorPrimaryDark));
            shareButt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                    whatsappIntent.setType("text/plain");
                    whatsappIntent.setPackage("com.whatsapp");
                    whatsappIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.fourthstatelabs.zchedule&hl=en");
                    try {
                        context.startActivity(whatsappIntent);
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(context, "Whatsapp is not installed", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            return rootViewSummary;
        }
        public void schedule_todo_notification(Notification_Holder n) {
            if (n.cal.getTimeInMillis() > System.currentTimeMillis()) {
                AlarmManager alarmManager = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);
                Intent intent = new Intent(getActivity(), NotifyService.class);
                Gson js = new Gson();
                String f = js.toJson(n);
                intent.putExtra("notificationContent", f);
                intent.putExtra("notificationCode",id);
                intent.putExtra("fromClass","WorkSpace");
                id++;
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getContext(), id, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                put(context,notificationIdentifier,id);//editor.putInt("identifier", id);
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, n.cal.getTimeInMillis(), pendingIntent);
            }
        }
        Calendar c;
        void showReminderSetter(final Switch reminderSwitch){
            Calendar now = Calendar.getInstance();
            DatePickerDialog dpd = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePickerDialog view, final int year, final int monthOfYear, final int dayOfMonth) {
                    TimePickerDialog tpd = TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
                            c=Calendar.getInstance();
                            c.set(year, monthOfYear, dayOfMonth, hourOfDay,minute);
                        }
                    },false);
                    tpd.show(getFragmentManager(),"reminderTime");
                    tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            reminderSwitch.setChecked(false);
                        }
                    });
                }
            }, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));

            dpd.show(getFragmentManager(),"reminderDate");
            dpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    reminderSwitch.setChecked(false);
                }
            });
        }

        void showCabinAlertDialog(final listAdapter_teachers cabinListAdapter) {
            final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
            final View alertCabinView = getActivity().getLayoutInflater().inflate(R.layout.floatingview_add_cabin, null);
            alertBuilder.setView(alertCabinView);
            final AlertDialog alert = alertBuilder.create();

            Toolbar addCabinToolbar =(Toolbar)alertCabinView.findViewById(R.id.alert_cabin_toolbar);
            addCabinToolbar.setBackgroundColor(getResources().getColor(ThemeProperty.colorPrimaryDark));
            addCabinToolbar.inflateMenu(R.menu.menu_add_todo);
            alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    workSpace.hideSoftKeyboard(getActivity());
                }
            });
            addCabinToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    int id = item.getItemId();
                    if(id==R.id.action_add_todo){
                        String name = ((TextView) alertCabinView.findViewById(R.id.alert_cabin_teacherName)).getText().toString();
                        String cabin = ((TextView) alertCabinView.findViewById(R.id.alert_cabin_cabinAddress)).getText().toString();
                        if (name.trim().equals("") || cabin.trim().equals("")) {
                            Toast.makeText(context, "Invalid Data !", Toast.LENGTH_LONG).show();
                        }
                        else {
                            for (int i = 0; i < DataContainer.cablist.size(); i++) {
                                if (DataContainer.cablist.get(i).name.toLowerCase().equals(name.toLowerCase())) {
                                    DataContainer.cablist.get(i).cabin = cabin;
                                    writeCabListToPrefs();
                                    cabinListAdapter.updatecontent(DataContainer.cablist);
                                    alert.cancel();
                                    return true;
                                }
                            }
                            Teacher c = new Teacher();
                            c.name = name;
                            c.cabin = cabin;
                            DataContainer.cablist.add(c);
                            DataContainer.toBeUpdated.add(c);
                            put(context,toUpdate,(new Gson()).toJson(DataContainer.toBeUpdated));                            writeCabListToPrefs();
                            cabinListAdapter.updatecontent(DataContainer.cablist);
                            alert.cancel();
                            return true;
                        }
                    }
                    return false;
                }
            });
            alert.show();
        }  //CREATE AND HANDLES THE ALERT DIALOG BOX TO ADD CABIN
        void setSearcher(final TextView searchBox, final listAdapter_teachers teacherAdapter, final ImageButton button) {
            Set<String> teacherNameSet = DataContainer.teachers.keySet();
            //if(teacherNameSet.size()>0) {
                adapter = new ArrayAdapter<>(context, R.layout.item_teacher_search, R.id.teacher_name, teacherNameSet.toArray(new String[teacherNameSet.size()]));
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(teacherSearch.getWindowToken(), 0);
                resultList.setAdapter(adapter);
                resultList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        String selectedTeacher = resultList.getItemAtPosition(i).toString();
                        searchBox.setText(selectedTeacher);
                        showTeacher(DataContainer.teachers.get(selectedTeacher), teacherAdapter);
                        resultList.setVisibility(View.GONE);
                    }
                });

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    teacherSearch.setText("");
                }
            });
                teacherSearch.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence search, int i, int i1, int i2) {
                        if (search.length() > 0) {
                            resultList.setVisibility(View.VISIBLE);
                            adapter.getFilter().filter(search);
                            button.setImageDrawable(getResources().getDrawable(R.drawable.ic_clear));
                        } else {
                            resultList.setVisibility(View.INVISIBLE);
                            button.setImageDrawable(getResources().getDrawable(R.drawable.ic_search));
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                    }
                });
            //}
        }


        void showTeacher(final Teacher teacher,final listAdapter_teachers teacherAdapter){
            final AlertDialog.Builder alertBuilder= new AlertDialog.Builder(context);;
            final View view= View.inflate(context,R.layout.floatingview_show_teacher,null);
            TextView teachername=((TextView)view.findViewById(R.id.show_teacher_name));
            final TextView editcabin=((TextView)view.findViewById(R.id.edit_teacher_cabin));
            teachername.setText(teacher.name);
            teachername.setTypeface(config.nunito_bold);
            final TextView cabin =(TextView)view.findViewById(R.id.show_teacher_cabin);
            cabin.setTypeface(config.nunito_reg);
            cabin.setText(teacher.cabin);
            alertBuilder.setView(view);
            final AlertDialog alertDialog = alertBuilder.create();

            (view.findViewById(R.id.alert_close)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.cancel();
                    workSpace.hideSoftKeyboard(getActivity());
                }
            });
            (view.findViewById(R.id.show_teacher_yes)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View Clickedview) {
                    (view.findViewById(R.id.wrong_information_tap)).setVisibility(View.INVISIBLE);
                }
            });
            (view.findViewById(R.id.show_teacher_no)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View viewClicked) {
                    //ACTIVITY TO EXECUTE IF THE GIVEN INFORMATION IS WRONG
                    view.findViewById(R.id.layout_edit_teacher).setVisibility(View.VISIBLE);
                    cabin.setVisibility(View.INVISIBLE);
                    editcabin.setText(teacher.cabin);
                    if(editcabin.requestFocus()) {
                        InputMethodManager iMM = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        iMM.showSoftInput(editcabin, InputMethodManager.SHOW_IMPLICIT);
                    }
                    (view.findViewById(R.id.wrong_information_tap)).setVisibility(View.INVISIBLE);
                }
            });
            (view.findViewById(R.id.save_teacher)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //CODE TO EXECUTE TO SAVE THE TEAHCER INFORMATION.
                    //SHOW EDITED ADDRESS TO THE TEACHER LISTVIEW
                    Teacher editedTeacher= new Teacher();
                    editedTeacher.cabin=editcabin.getText().toString();
                    editedTeacher.name=teacher.name;
                    DataContainer.cablist.add(editedTeacher);
                    DataContainer.toBeUpdated.add(editedTeacher);
                    put(context,toUpdate,(new Gson()).toJson(DataContainer.toBeUpdated));                    teacherAdapter.updatecontent(DataContainer.cablist);
                    workSpace.writeCabListToPrefs();
                    alertDialog.cancel();
                    //SHOW THAT WE WILL UPDATE THE DATABASE SOON
                }
            });
            alertDialog.show();
        }

        LinearLayout taskGridLeft, taskGridRight;

        void applyGrid(final int i){
            if(i>= DataContainer.notes.size()) return;
            else{
                int leftHeight=taskGridLeft.getMeasuredHeight();
                int rightHeight =taskGridRight.getMeasuredHeight();
                View viewToAdd=getTaskView(i);
                setTaskOnClick(viewToAdd,i);
                if(leftHeight<=rightHeight){
                    taskGridLeft.addView(viewToAdd);
                }
                else{
                    taskGridRight.addView(viewToAdd);
                }
                viewToAdd.post(new Runnable() {
                    @Override
                    public void run() {
                        applyGrid(i+1);
                    }
                });
            }
        }

        void populateTaskGrid() {
            taskGridLeft.removeAllViews();
            taskGridRight.removeAllViews();
            if(DataContainer.notes.size()>0) {
                noNotesText.setVisibility(View.GONE);
                View viewToAdd = getTaskView(0);
                setTaskOnClick(viewToAdd, 0);
                taskGridLeft.addView(viewToAdd);
                viewToAdd.post(new Runnable() {
                    @Override
                    public void run() {
                        applyGrid(1);
                    }
                });
            }
            else{
                noNotesText.setVisibility(View.VISIBLE);
            }
        }
        AlertDialog expanded;
        void setTaskOnClick(View view,final int index){
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder alertDialogBuilder= new AlertDialog.Builder(context);
                    alertDialogBuilder.setView(getExpanded(index));
                    expanded= alertDialogBuilder.create();
                    expanded.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    expanded.show();
                    expanded.getWindow().setLayout(((int)(config.width*0.8)),RelativeLayout.LayoutParams.WRAP_CONTENT);
                }
            });
        }

        int[] colors = new int[]{R.color.dot_light_screen1,R.color.dot_light_screen5, R.color.dot_light_screen2,R.color.dot_light_screen3,R.color.dot_light_screen4};

        View getTaskView(final int index) {
            final Notification_Holder cTask = DataContainer.notes.get(index);
            final View taskView = getActivity().getLayoutInflater().inflate(R.layout.course_task_card_view, null);
            TextView title =((TextView) taskView.findViewById(R.id.task_title));
            ImageButton edit=(ImageButton) taskView.findViewById(R.id.task_edit);
            title.setTypeface(config.nunito_bold);
            title.setText(cTask.title);
            TextView deadLineTextView= (TextView) taskView.findViewById(R.id.task_deadLine);
            deadLineTextView.setTypeface(config.nunito_reg);
            TextView desc=((TextView) taskView.findViewById(R.id.task_desc));
            desc.setTypeface(config.nunito_reg);
            desc.setText(cTask.content);
            (taskView.findViewById(R.id.task_delete)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    delTask(cTask);
                    populateTaskGrid();
                }
            });

            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editTask(index);
                }
            });
            Calendar deadLine = cTask.cal;
            deadLineTextView.setText(getDateTimeString(deadLine));
            //taskView.setBackground(getResources().getDrawable(R.drawable.soft_corner_taskview));
            GradientDrawable softShape = (GradientDrawable) taskView.getBackground();
            final int colorIndex = index % (colors.length);
            softShape.setColor(getResources().getColor(colors[colorIndex]));
            return taskView;
        }

        View getExpanded(final int index){
            final Notification_Holder cTask = DataContainer.notes.get(index);
            final View taskView = getActivity().getLayoutInflater().inflate(R.layout.expanded_task_card_view, null);
            TextView title =((TextView) taskView.findViewById(R.id.task_title));
            ImageButton edit=(ImageButton) taskView.findViewById(R.id.task_edit);
            title.setTypeface(config.nunito_bold);
            title.setText(cTask.title);
            TextView deadLineTextView= (TextView) taskView.findViewById(R.id.task_deadLine);
            deadLineTextView.setTypeface(config.nunito_reg);
            TextView desc=((TextView) taskView.findViewById(R.id.task_desc));
            desc.setTypeface(config.nunito_reg);
            desc.setText(cTask.content);
            (taskView.findViewById(R.id.task_delete)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try{expanded.cancel();}
                    catch (Exception e){}
                    delTask(cTask);
                    populateTaskGrid();
                    }
            });

            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editTask(index);
                }
            });
            Calendar deadLine = cTask.cal;
            deadLineTextView.setText(getDateTimeString(deadLine));
            //taskView.setBackground(getResources().getDrawable(R.drawable.soft_corner_taskview));
            GradientDrawable softShape = (GradientDrawable) taskView.getBackground();
            final int colorIndex = index % (colors.length);
            softShape.setColor(getResources().getColor(colors[colorIndex]));
            return taskView;
        }

        void editTask(final int index){
            Notification_Holder n= DataContainer.notes.get(index);
            final AlertDialog alert;
            View root = getActivity().getLayoutInflater().inflate(R.layout.floatingview_add_todo, null);
            final EditText title = (EditText) root.findViewById(R.id.title);
            final EditText other = (EditText) root.findViewById(R.id.note);
            final Switch reminderSwitch =(Switch)root.findViewById(R.id.add_todo_reminder_switch);
            Toolbar addTaskToolbar=((Toolbar)root.findViewById(R.id.add_task_toolbar));
            addTaskToolbar.inflateMenu(R.menu.menu_add_todo);
            title.setText(n.title);
            other.setText(n.content);

            AlertDialog.Builder bui = new AlertDialog.Builder(context);
            bui.setView(root);
            alert = bui.create();
            alert.show();

            reminderSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                    if(checked) {
                        c = null;
                        showReminderSetter(reminderSwitch);
                    }
                    else {
                        c = null;
                        reminderSwitch.setText("Set Reminder");
                    }
                }
            });
            addTaskToolbar.setTitle("Edit Note");
            addTaskToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    int id = item.getItemId();
                    if(id==R.id.action_add_todo) {
                        if (!title.getText().toString().isEmpty() && !other.getText().toString().isEmpty() ) {
                            Notification_Holder n;
                            if(c!=null) {
                                n = new Notification_Holder(c, title.getText().toString(), other.getText().toString(),"You have a deadline to meet");
                                schedule_todo_notification(n);
                            }
                            else
                                n = new Notification_Holder(Calendar.getInstance(), title.getText().toString(), other.getText().toString(),"You have a deadline to meet");
                            updateTask(n,index);
                            Gson json = new Gson();
                            String temporary = json.toJson(DataContainer.notes);
                            put(context,todolist,temporary);//editor.putString("todolist", temporary);
                            int indexOfView=index/2;
                            if (index % 2 != 0) {
                                View view = getTaskView(index);
                                setTaskOnClick(view,index);
                                taskGridRight.removeViewAt(indexOfView);
                                taskGridRight.addView(view,indexOfView);
                            } else {
                                View view = getTaskView(index);
                                setTaskOnClick(view,index);
                                taskGridLeft.removeViewAt(indexOfView);
                                taskGridLeft.addView(view,indexOfView);
                            }
                            expanded.cancel();
                            workSpace.hideSoftKeyboard(getActivity());
                            alert.cancel();
                        } else
                            Toast.makeText(getContext(), "Both title and note must contain some text", Toast.LENGTH_SHORT).show();
                        return true;
                    }
                    else {
                        workSpace.hideSoftKeyboard(getActivity());
                        alert.cancel();
                    }
                    return false;
                }
            });
        }


        void delTask(Notification_Holder task) {
            AlarmManager alarmManager = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(getActivity(), NotifyService.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(getContext(), task.id, intent, PendingIntent.FLAG_CANCEL_CURRENT);
            alarmManager.cancel(pendingIntent);
            DataContainer.notes.remove(task);
            put(context,todolist,Notification_Holder.convert_to_jason(DataContainer.notes));//editor.putString("todolist", Notification_Holder.convert_to_jason(config.notes));
        }

        void updateTask(Notification_Holder newtask, int index){
            Notification_Holder oldTask= DataContainer.notes.get(index);
            AlarmManager alarmManager = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(getActivity(), NotifyService.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(getContext(), oldTask.id, intent, PendingIntent.FLAG_CANCEL_CURRENT);
            alarmManager.cancel(pendingIntent);
            DataContainer.notes.set(index,newtask);
            put(context,todolist,Notification_Holder.convert_to_jason(DataContainer.notes));//editor.putString("todolist", Notification_Holder.convert_to_jason(config.notes));
        }

    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            return 4;
        }



    }

    public static void writeCabListToPrefs() {
        Gson json=new Gson();
        String cabListJson=json.toJson(DataContainer.cablist);
        put(context,customTeachers,cabListJson);//cabListEditor.putString("customTeachers",cabListJson);
    } //SAVE THE CONTENT OF CABLIST TO THE PREFERENCES

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        try {
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
        catch (Exception e){ /*KEYBOARD HIDE FAILED*/ }
    }  //HIDES THE KEYBOARD

    public static void setOnTouchListener(View view, final Activity activity) {
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(activity);
                    if (resultList.getVisibility()==View.VISIBLE){
                        resultList.setVisibility(View.INVISIBLE);
                    }
                    return false;
                }
            });
        }

        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setOnTouchListener(innerView,activity);
            }
        }
    }  //SET THE TOUCH TO HIDE THE KEYBOARD

    public static void setOnViewTouchListener(View view,final Activity activity){
        view.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                hideSoftKeyboard(activity);
                if (resultList.getVisibility()==View.VISIBLE){
                    resultList.setVisibility(View.INVISIBLE);
                }
                return false;
            }
        });
    }

    void getDimensions(){
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        config.width=dm.widthPixels;
        config.height=dm.heightPixels;
    }  //GET THE DIMENSIONS OF THE DEVICE

    public static String getDateTimeString(Calendar deadLine){
        int today = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
        int taskDay= deadLine.get(Calendar.DAY_OF_YEAR);
        String dateString;
        String timeString = getHour(deadLine.get(Calendar.HOUR))+":"+getMinute(deadLine.get(Calendar.MINUTE))+getAMPM(deadLine.get(Calendar.AM_PM));
        if(today==taskDay){
            dateString= "Today";
        }
        else if(today==taskDay-1){
            dateString="Tomorrow";
        }
        else if(today==taskDay+1){
            dateString="Yesterday";
        }
        else{
            dateString=deadLine.get(Calendar.DATE) + "/" + (deadLine.get(Calendar.MONTH) + 1) + "/" + deadLine.get(Calendar.YEAR);
        }

        return (dateString+" "+timeString);
    }

    static String getAMPM(int AMPM){
        if(AMPM==0)
            return "AM";
        else
            return "PM";
    }

    static String getMinute(int minute){
        if(minute<10){
            return ("0"+minute);
        }
        return String.valueOf(minute);
    }

    static String getHour(int hour){
        if(hour==0) return "12";
        else return String.valueOf(hour);
    }

    public static void updateSearcher(){
        Set<String> teacherNameSet = DataContainer.teachers.keySet();
        adapter = new ArrayAdapter<>(context, R.layout.item_teacher_search, R.id.teacher_name, teacherNameSet.toArray(new String[teacherNameSet.size()]));
        resultList.setAdapter(adapter);

    }
}