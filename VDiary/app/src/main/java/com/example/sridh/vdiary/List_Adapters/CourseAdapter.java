package com.example.sridh.vdiary.List_Adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.sridh.vdiary.Classes.Subject;
import com.example.sridh.vdiary.Classes.themeProperty;
import com.example.sridh.vdiary.R;
import com.example.sridh.vdiary.config;

import java.util.List;

import static com.example.sridh.vdiary.config.getCurrentTheme;

/**
 * Created by Sparsha Saha on 9/11/2016.
 */
    public class CourseAdapter extends RecyclerView.Adapter<CourseAdapter.ViewHolder>{

        List<Subject> list;
        Context context;
        themeProperty ThemeProperty;
        private int lastPosition = -1;
    OnItemClickListener onItemClickListener;

        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView subname, teachername,attendance,type;
            public ViewHolder(View rowview) {
                super(rowview);
                subname=(TextView)rowview.findViewById(R.id.course_title);
                attendance=(TextView)rowview.findViewById(R.id.course_attendance);
                teachername=(TextView)rowview.findViewById(R.id.course_teacher);
                type=(TextView)rowview.findViewById(R.id.course_type);
            }
        }
        public CourseAdapter(Context t, List<Subject> l){
            context=t;
            list=l;
            ThemeProperty= getCurrentTheme();
        }
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = View.inflate(context,R.layout.rowview_course,null);
            return new ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            final int pos = position;
            final Subject sub=list.get(position);
            holder.subname.setTypeface(config.nunito_bold);
            holder.teachername.setTypeface(config.nunito_reg);
            holder.attendance.setTypeface(config.nunito_reg);
            holder.type.setTypeface(config.nunito_reg);
            GradientDrawable shoftShape = (GradientDrawable)holder.type.getBackground();
            shoftShape.setColor(context.getResources().getColor(ThemeProperty.colorPrimary));
            //set items
            holder.subname.setText(sub.title);
            holder.teachername.setText(sub.teacher);
            holder.attendance.setText(sub.attString);
            holder.type.setText(sub.type);
            int attendance=Integer.parseInt(sub.attString.substring(0,sub.attString.indexOf('%')));
            if(attendance<75){
                holder.attendance.setTextColor(Color.RED);
            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(onItemClickListener!=null)
                        onItemClickListener.onItemClick(sub,pos);
                }
            });
            //setAnimation(holder.itemView, position);
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        private void setAnimation(View viewToAnimate, int position)
        {
            if (position > lastPosition)
            {
                Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
                viewToAnimate.startAnimation(animation);
                lastPosition = position;
            }
        }

        public interface OnItemClickListener{
            void onItemClick(Subject subject,int position);
        }

        public void setOnItemClickListener(OnItemClickListener onItemClickListener){
            this.onItemClickListener=onItemClickListener;
        }

        public void change(int position){
            notifyDataSetChanged();
        }
    }
