package com.example.sridh.vdiary.Utils;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;

import com.example.sridh.vdiary.Classes.Credential;
import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.example.sridh.vdiary.Utils.prefs.SERVER_URL;
import static com.example.sridh.vdiary.Utils.prefs.get;

/**
 * Created by sid on 6/13/17.
 */

public class HttpRequest {
    private String url;
    private Map<String,String> params;
    private static String herokuBaseUrl_server1 = "https://zchedule-server1.herokuapp.com";
    private static String herokuBaseUrl_server2 = "https://zchedule-server2.herokuapp.com";
    public static String[] servers = {herokuBaseUrl_server1,herokuBaseUrl_server2};
    private String authentication =null;
    private Context context;
    public HttpRequest(Context context,String url){
        this.url=get(context,SERVER_URL,herokuBaseUrl_server1)+url;
        params= new HashMap<>();
        this.context=context;
    }

    public void addParam(String key,String value){
        params.put(key,value);
    }

    public HttpRequest addAuthenticationHeader(String reg,String password){
        Credential creds = new Credential(reg,password);
        this.authentication= new Gson().toJson(creds);
        return this;
    }

    public interface OnResponseListener{
        void OnResponse(String response);
    }

    public HttpRequest sendRequest(OnResponseListener onResponseListener){
        (new ApiExecuter(onResponseListener)).execute();
        return this;
    }

    public static void getAll(Activity activity,String regno, String password, final OnResponseListener onResponseListener){
        new HttpRequest(activity,"/all")
        .addAuthenticationHeader(regno,password)
        .sendRequest(new OnResponseListener() {
            @Override
            public void OnResponse(String response) {
                onResponseListener.OnResponse(response);
            }
        });
    }

    private class ApiExecuter extends AsyncTask<Void,Void,String> {

        OnResponseListener onResponseListener;
        public ApiExecuter(OnResponseListener onResponseListener){
            this.onResponseListener=onResponseListener;
        }
        @Override
        protected String doInBackground(Void... voids) {
            OkHttpClient client = new OkHttpClient();
            url=url+"?";
            for (String key: params.keySet()){
                url=url+key+"="+params.get(key)+"&";
            }
            Request.Builder requestBuilder= new Request.Builder()
                    .url(url)
                    .post(RequestBody.create(null,new byte[0]));
            if(authentication!=null)
                    requestBuilder.addHeader("Authorization", authentication);

            Request request = requestBuilder.build();
            Response response = null;
            try {
                client.setConnectTimeout(30, TimeUnit.SECONDS);
                client.setReadTimeout(30,TimeUnit.SECONDS);
                client.setWriteTimeout(30,TimeUnit.SECONDS);
                response = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(response!=null && response.isSuccessful()){
                try {
                    return response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else{
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            onResponseListener.OnResponse(s);
            super.onPostExecute(s);
        }
    }

}
